/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 17:48:21 by clrichar          #+#    #+#             */
/*   Updated: 2017/10/29 14:14:26 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*node;
	t_list	*tmp;

	new = NULL;
	while (lst != NULL)
	{
		node = (*f)(lst);
		if (new == NULL)
			new = node;
		else
			tmp->next = node;
		tmp = node;
		lst = lst->next;
	}
	return (new);
}
