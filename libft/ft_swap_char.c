/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap_char.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 10:58:25 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/15 10:58:44 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_swap_char(char *str, int start, int end)
{
	char		tmp;
	char		*ret;

	ret = str;
	tmp = ret[start];
	ret[start] = ret[end];
	ret[end] = tmp;
	return (ret);
}
