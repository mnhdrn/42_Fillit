/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/05 11:29:24 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/18 12:10:58 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int					main(int ac, char **av)
{
	int				valid;
	t_struct		*data;

	valid = 0;
	if (ac != 2)
		ft_putstr("usage: fillit filename\n");
	else if (ac == 2)
	{
		data = ft_data_sorter(av);
		valid = ft_parser(data);
		if (valid != 0)
			ft_putstr("error\n");
		else
		{
			ft_clean_data(data);
			ft_solver(data, ft_map(data));
		}
	}
	else
		ft_putstr("error\n");
	return (0);
}
