/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/05 15:25:11 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/18 17:57:16 by erwberna         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdbool.h>
# include "libft.h"

# define ISPIECE(x) (x == '.' || x == '#')

typedef struct			s_struct {
	char				**tab;
	int					nb;
	bool				is_printed;
	int					x;
	int					y;
}						t_struct;

t_struct				*ft_data_sorter(char **av);
t_struct				*ft_clean_data(t_struct *data);
int						ft_parser(t_struct *data);
char					**ft_map_grow(char **map, size_t size);
char					**ft_map(t_struct *data);
int						ft_nb_piece(t_struct *data);
void					ft_solver(t_struct *data, char **map);

#endif
