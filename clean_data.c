/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_data.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/16 11:50:28 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/18 22:12:07 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static char			**ft_clean_col_front(char **tab)
{
	int				i;
	int				count;
	size_t			size;

	i = 0;
	count = 0;
	size = ft_strlen(tab[0]);
	while (tab[i] != NULL)
	{
		if (tab[i][0] == '.')
			count++;
		i++;
	}
	if (count == i)
	{
		i = 0;
		while (tab[i] != NULL)
		{
			tab[i] = ft_strsub(tab[i], 1, size);
			i++;
		}
	}
	return (tab);
}

static char			**ft_clean_col_back(char **tab)
{
	int				i;
	int				count;
	size_t			size;

	i = 0;
	count = 0;
	size = ft_strlen(tab[0]);
	while (tab[i] != NULL)
	{
		if (tab[i][size - 1] == '.')
			count++;
		i++;
	}
	if (count == i)
	{
		i = 0;
		while (tab[i] != NULL)
		{
			tab[i] = ft_strsub(tab[i], 0, (size - 1));
			i++;
		}
	}
	return (tab);
}

static char			**ft_clean_line(char **tab)
{
	int				i;
	int				j;

	i = 0;
	while (tab[i] != NULL)
	{
		if (!(ft_strncmp(tab[i], "....", ft_strlen(tab[i]))))
		{
			j = i;
			while (tab[j])
			{
				tab[j] = tab[j + 1];
				j++;
			}
		}
		i++;
	}
	return (tab);
}

t_struct			*ft_clean_data(t_struct *data)
{
	int				i;
	int				j;

	i = 0;
	while (data[i].tab != NULL)
	{
		j = 0;
		while (j < 4)
		{
			ft_clean_col_front(data[i].tab);
			ft_clean_col_front(data[i].tab);
			ft_clean_col_back(data[i].tab);
			ft_clean_col_back(data[i].tab);
			ft_clean_line(data[i].tab);
			j++;
		}
		i++;
	}
	return (data);
}
