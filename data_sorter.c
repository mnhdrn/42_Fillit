/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_sorter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/18 22:12:25 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/24 20:10:08 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static size_t			ft_countpiece(char *s)
{
	size_t				len;
	int					exist;

	len = 0;
	exist = 0;
	while (*s)
	{
		if (*s == '\n')
			exist++;
		if (*(s + 1) != '\0' && (*s == '\n' && *(s + 1) == '\n' &&
					s[2] != '\n'))
			len++;
		else if (*(s + 1) != '\0' && (*s == '\n' && *(s + 1) == '\n' &&
					s[2] == '\n'))
			return (0);
		s++;
	}
	if (exist == 0 && len == 0)
		return (0);
	else
		return (len + 1);
}

static char				*ft_get_str(char **av)
{
	int					fd;
	int					size;
	char				buffer[1024 + 1];
	char				*read_content;

	fd = open(av[1], O_RDONLY);
	if (!(read_content = ft_strnew(1)))
		return (NULL);
	while ((size = read(fd, buffer, 1024)) > 0)
	{
		buffer[size] = '\0';
		if (!(read_content = ft_strjoin(read_content, buffer)))
			return (NULL);
	}
	close(fd);
	return (read_content);
}

static char				**ft_split_to_tab(char *s, int nb)
{
	char				**ret;
	char				*piece;
	int					piece_nb;

	piece_nb = ((nb - 1) + (nb * 20)) - 20;
	if (!(piece = (char *)malloc(sizeof(char) * 22)))
		return (NULL);
	piece = ft_strsub(s, piece_nb, 21);
	ret = ft_strsplit(piece, '\n');
	free(piece);
	return (ret);
}

static t_struct			*ft_split_to_struct(char *s)
{
	t_struct			*ret;
	size_t				size;
	size_t				i;

	size = ft_countpiece(s);
	if (!(ret = (t_struct *)ft_memalloc(sizeof(t_struct) * (size + 1))))
		return (NULL);
	i = 0;
	while (i < size)
	{
		ret[i].tab = ft_split_to_tab(s, (i + 1));
		ret[i].nb = i;
		i++;
	}
	ret[i].tab = NULL;
	ret[i].nb = -1;
	return (ret);
}

t_struct				*ft_data_sorter(char **av)
{
	char				*str;
	t_struct			*ret;

	ret = NULL;
	str = ft_get_str(av);
	ret = ft_split_to_struct(str);
	free(str);
	return (ret);
}
