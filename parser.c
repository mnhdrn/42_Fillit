/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/08 15:51:28 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/24 20:04:45 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int			ft_check_connect(int i, int j, char **tab)
{
	int				nb;

	nb = 0;
	(j > 0 && tab[i][j - 1] == '#') ? nb++ : 0;
	(j < 4 && tab[i][j + 1] == '#') ? nb++ : 0;
	(i > 0 && tab[i - 1][j] == '#') ? nb++ : 0;
	(tab[i + 1] != NULL && tab[i + 1][j] == '#') ? nb++ : 0;
	return (nb);
}

static int			ft_check_str(char *s)
{
	while (*s)
	{
		if (!(ISPIECE(*s)))
			return (-1);
		s++;
	}
	return (0);
}

static int			ft_check_diese(char **tab)
{
	int				i;
	int				j;
	int				nb;
	int				connect;

	i = 0;
	nb = 0;
	connect = 0;
	while (tab[i] != NULL)
	{
		j = 0;
		while (tab[i][j])
		{
			if (tab[i][j] == '#')
			{
				connect = connect + ft_check_connect(i, j, tab);
				nb++;
			}
			j++;
		}
		i++;
	}
	return ((nb != 4) ? -1 : connect);
}

static int			ft_check_tab(char **tab)
{
	int				i;
	int				nb;

	i = 0;
	nb = 0;
	while (tab[i] != NULL)
	{
		if (ft_strlen(tab[i]) != 4 || ft_check_str(tab[i]) != 0)
			return (-1);
		else if (i < 5)
			i++;
		else
			return (-1);
	}
	if (i != 4)
		return (1);
	return (0);
}

int					ft_parser(t_struct *data)
{
	int				i;
	int				check_str;
	int				check_diese;

	i = 0;
	check_diese = 0;
	if (data[i].tab == NULL)
		return (-1);
	while (data[i].tab != NULL)
	{
		check_str = ft_check_tab(data[i].tab);
		if (check_str != 0)
			return (-1);
		check_diese = ft_check_diese(data[i].tab);
		if (check_diese < 6 || check_diese > 12)
			return (-1);
		i++;
	}
	if (i > 26)
		return (-1);
	return (0);
}
