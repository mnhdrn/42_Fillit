# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/06 14:36:02 by clrichar          #+#    #+#              #
#    Updated: 2017/11/24 19:55:46 by clrichar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			:=			fillit

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  FILES                                       #

SRC				:=			main.c			\
							data_sorter.c	\
							parser.c		\
							clean_data.c	\
							map.c			\
							solver.c		

OBJ				:=			$(SRC:.c=.o)


L_FT			:=			./libft/

LIB				:=			./libft/libft.a
#==============================================================================#
#------------------------------------------------------------------------------#
#                            COMPILER & FLAGS                                  #

CFLAGS			:=			-Wall -Wextra -Werror -Ilibft.h -Ifillit.h 

#------------------------------------------------------------------------------#
#                                 RULES                                        #

all:					$(NAME)

$(NAME):				$(LIB) $(OBJ)
	$(CC) $(OBJ) $(CFLAGS) ./libft/libft.a -o $(NAME) 
	 @printf '\033[33m[ ▴ ] %s\n\033[0m' "Compilation of $(NAME) is done ---"¬

$(LIB):
	@make -C $(L_FT)

clean:
	@make -C $(L_FT) clean
	rm -f $(OBJ)

fclean: 				clean
	rm -rf $(NAME)
	@make -C $(L_FT) fclean --no-print-directory
	@printf '\033[33m[ ▴ ] %s\n\033[0m' "Fclean of $(NAME) is done ---"¬

re:						fclean all

.PHONY: all clean fclean re
